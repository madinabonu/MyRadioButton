package com.example.madina.myradiobutton;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Madina-PC on 22.10.2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<Model> list;
    private Model data;
    private int last = -1;
    static final String[] colors = {
            "red",
            "orange",
            "yellow",
            "green",
            "blue",
            "indigo",
            "violet"
    };

    public MyAdapter(List<Model> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        data = list.get(position);

        holder.text.setText("SomeText");
        holder.image.setImageResource(list.get(position).getImage());
        holder.itemView.setBackgroundColor(data.getColor());
        Log.i("madina",""+position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (last != -1) {
                    list.get(last).setImage(R.drawable.ic_radio_button_off);
                }
                list.get(position).setImage(R.drawable.ic_radio_button_on);
                last = position;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            text = (TextView) itemView.findViewById(R.id.text);
        }
    }
}
