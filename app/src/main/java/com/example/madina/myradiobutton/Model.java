package com.example.madina.myradiobutton;

/**
 * Created by Madina-PC on 22.10.2017.
 */

public class Model {
    private int image;
    private String text;
    private int color;

    public Model(int image, String text,int color) {
        this.image = image;
        this.text = text;
        this.color = color;
    }

    public int getImage() {
        return image;
    }

    public int getColor() {
        return color;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
