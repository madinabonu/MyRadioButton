package com.example.madina.myradiobutton;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView view;
    List<Model> list;
    static final String[] colors = {
            "#FF0000",
            "#FF7F00",
            "#FFFF00",
            "#00FF00",
            "#0000FF",
            "#4B0082",
            "#9400D3"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = new ArrayList<>();
        for(int i = 0; i < 100; i ++)
        {
            list.add(new Model(R.drawable.ic_radio_button_off,"SomeText", Color.parseColor(colors[i % colors.length])));
        }


        view = (RecyclerView) findViewById(R.id.recyclerview);
        MyAdapter adapter = new MyAdapter(list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        view.setLayoutManager(layoutManager);

        view.setAdapter(adapter);



    }

}
